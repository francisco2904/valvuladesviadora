from PyQt5.QtWidgets import QApplication, QMainWindow, QPushButton, QVBoxLayout, QWidget, QLineEdit, QLabel, QHBoxLayout, QSpinBox, QFormLayout, QMessageBox, QFileDialog
from PyQt5.QtCore import pyqtSlot
import sys

class DTMCalculator(QMainWindow):
    def __init__(self):
        super().__init__()
        self.scale_readings = []  # Stores user inputs for scale readings (masses).
        self.time_inputs = []  # Stores user inputs for times.
        self.flow_rate_inputs = []  # Stores user inputs for flow rates.
        self.initUI()

    def initUI(self):
        self.setWindowTitle('DTM Calculator')
        main_layout = QVBoxLayout()

        form_layout = QFormLayout()
        self.q0_input = QLineEdit(self)
        form_layout.addRow('Enter Q0 (flow rate [m³/h]):', self.q0_input)
        
        self.tm_input = QLineEdit(self)
        form_layout.addRow('Enter TM (total time [s]):', self.tm_input)
        
        self.m0_input = QLineEdit(self)
        form_layout.addRow('Enter m0 (scale reading [kg]):', self.m0_input)

        self.n_input = QSpinBox(self)
        self.n_input.setMinimum(1)
        self.n_input.setMaximum(100)
        self.n_input.valueChanged.connect(self.updateInputFields)
        form_layout.addRow('Enter N (number of increments):', self.n_input)

        self.inputs_layout = QVBoxLayout()
        form_layout.addRow('Incremental Measurements (mi, ti, Flow rate i):', self.inputs_layout)

        main_layout.addLayout(form_layout)

        self.calculate_button = QPushButton('Calculate DTM', self)
        self.calculate_button.clicked.connect(self.on_click)

        self.calculate_button.clicked.connect(self.on_click)
        main_layout.addWidget(self.calculate_button)

        self.result_label = QLabel('DTM (time error [s]):')
        self.result_output = QLineEdit(self)
        self.result_output.setReadOnly(True)
        main_layout.addWidget(self.result_label)
        main_layout.addWidget(self.result_output)

        self.save_button = QPushButton('Save Data', self)
        self.save_button.clicked.connect(self.saveData)
        main_layout.addWidget(self.save_button)

        self.open_button = QPushButton('Open Data', self)  # Corrected variable name
        self.open_button.clicked.connect(self.loadData)  # Now using self.open_button
        main_layout.addWidget(self.open_button)

        central_widget = QWidget()
        central_widget.setLayout(main_layout)
        self.setCentralWidget(central_widget)

        self.n_input.setValue(2)  # Set initial number of increments to 2 for demonstration.

    def updateInputFields(self, n):
        while self.inputs_layout.count():
            child = self.inputs_layout.takeAt(0)
            if child.widget():
                child.widget().deleteLater()

        self.scale_readings = []
        self.time_inputs = []
        self.flow_rate_inputs = []

        for i in range(n):
            scale_reading_input = QLineEdit(self)
            self.scale_readings.append(scale_reading_input)
            
            time_input = QLineEdit(self)
            self.time_inputs.append(time_input)
            
            flow_rate_input = QLineEdit(self)
            self.flow_rate_inputs.append(flow_rate_input)
            
            h_layout = QHBoxLayout()
            h_layout.addWidget(QLabel(f'm{i+1} [kg]:'))
            h_layout.addWidget(scale_reading_input)
            h_layout.addWidget(QLabel(f't{i+1} [s]:'))
            h_layout.addWidget(time_input)
            h_layout.addWidget(QLabel(f'Flow Rate{i+1} [m³/h]:'))
            h_layout.addWidget(flow_rate_input)
            self.inputs_layout.addLayout(h_layout)


    @pyqtSlot()
    def on_click(self):
        try:
            TM = float(self.tm_input.text())  # Total time of complete filling in seconds.
            N = self.n_input.value()  # Number of incremental steps.
            Q0 = float(self.q0_input.text())  # Average flow rate during complete filling in m³/h, no conversion needed.
            m0 = float(self.m0_input.text())  # Mass collected during complete filling in kg.

            # Ensure there are inputs for scale readings, times, and flow rates for each increment
            if not (len(self.scale_readings) == len(self.time_inputs) == len(self.flow_rate_inputs) == N):
                raise ValueError("Please ensure all fields for each increment are filled.")

            scale_masses = [float(input_field.text()) for input_field in self.scale_readings]
            times = [float(input_field.text()) for input_field in self.time_inputs]
            flow_rates = [float(input_field.text()) for input_field in self.flow_rate_inputs]  # Flow rates in m³/h, no conversion.

            # Calculate average flow rate during incremental fillings QN in m³/h.
            QN = sum(flow_rates) / N

            # Calculate deltas for mass.
            delta_ms = [scale_masses[i] - (scale_masses[i - 1] if i > 0 else 0) for i in range(N)]

            # Calculate DTM using the formula directly with Q0 and QN in m³/h.
            sum_delta_ms = sum(delta_ms)
            sum_times = sum(times)
            DTM = (TM / (N - 1)) * (((Q0 / QN) * (sum_delta_ms / sum_times)) / (m0 / TM) - 1)

            self.result_output.setText(f'DTM: {DTM:.9f} seconds')

        except Exception as e:
            print(f"An error occurred: {e}")
            self.result_output.setText('')




    def saveData(self):
        filePath, _ = QFileDialog.getSaveFileName(self, "Save File", "", "Text Files (*.txt)")
        if filePath:
            with open(filePath, 'w') as file:
                file.write(f'{self.q0_input.text()}\n{self.tm_input.text()}\n{self.m0_input.text()}\n{self.n_input.value()}\n')
                for scale_input, time_input, flow_rate_input in zip(self.scale_readings, self.time_inputs, self.flow_rate_inputs):
                    file.write(f'{scale_input.text()},{time_input.text()},{flow_rate_input.text()}\n')


    def loadData(self):
        filePath, _ = QFileDialog.getOpenFileName(self, "Open File", "", "Text Files (*.txt)")
        if filePath:
            with open(filePath, 'r') as file:
                q0, tm, m0, n = [next(file).strip() for _ in range(4)]
                self.q0_input.setText(q0)
                self.tm_input.setText(tm)
                self.m0_input.setText(m0)
                self.n_input.setValue(int(n))
                self.updateInputFields(int(n))  # Recreate input fields.
                for line, (scale_input, time_input, flow_rate_input) in zip(file, zip(self.scale_readings, self.time_inputs, self.flow_rate_inputs)):
                    m, t, flow_rate = line.strip().split(',')
                    scale_input.setText(m)
                    time_input.setText(t)
                    flow_rate_input.setText(flow_rate)


def main():
    app = QApplication(sys.argv)
    ex = DTMCalculator()
    ex.show()
    sys.exit(app.exec_())

if __name__ == '__main__':
    main()
